# Sorts

Working through various sorting algorithms in Clojure.

Currently, there is:
* [merge sort](http://en.wikipedia.org/wiki/Merge_sort)

Eventually, each of the following will be covered:
* [bubble sort](http://en.wikipedia.org/wiki/Bubblesort)
* [insertion sort](http://en.wikipedia.org/wiki/Insertion_sort)
* [heap sort](http://en.wikipedia.org/wiki/Heapsort)
* [quicksort](http://en.wikipedia.org/wiki/Quicksort)
* [timsort](http://en.wikipedia.org/wiki/Timsort)

## License

Copyright © 2012 Dan Grabowski

Distributed under the Eclipse Public License.
