(ns sorts.merge-test
  (:use clojure.test
        sorts.merge))

(deftest merging
  (testing "Empty inputs returns empty result."
    (is (= (seq []) (merge-sorted [] []))))
  (testing "Empty first input returns second input."
    (is (= (seq [1 2 3]) (merge-sorted [] [1 2 3]))))
  (testing "Merging interleaves values."
    (is (= (seq [1 2 3 4 5 6]) (merge-sorted [1 3 5] [2 4 6]))))
  (testing "Longer left list."
    (is (= (seq [1 2 3 4 5 6]) (merge-sorted [1 3 5 6] [2 4]))))
  (testing "Longer right list."
    (is (= (seq [1 2 3 4 5 6]) (merge-sorted [1 3] [2 4 5 6]))))
  (testing "Equal values in inputs."
    (is (= (seq [1 2 2 3 3 7]) (merge-sorted [1 2 3] [2 3 7]))))
  (testing "Equal subsequences in inputs."
    (is (= (seq [1 2 2 3 3 4 7]) (merge-sorted [1 2 2 4] [3 3 7])))))

(deftest sorting
  (testing "Already sorted returned unchanged."
    (is (= (seq [1 2 4 4 7 9]) (merge-sort [1 2 4 4 7 9]))))
  (testing "Empty input returns nil."
    (is (nil? (merge-sort []))))
  (testing "One item input returned unchanged."
    (is (= (seq [78]) (merge-sort [78]))))
  (testing "An unsorted input returned in order."
    (is (= (seq [1 2 3 4 5 6 7]) (merge-sort [4 3 1 2 7 5 6]))))
  (testing "An unsorted input with repeats returned in order."
    (is (= (seq [1 2 3 4 4 5 6 7]) (merge-sort [4 1 2 4 7 3 6 5])))))
