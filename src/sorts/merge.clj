(ns sorts.merge)

(defn merge-sorted [left right]
  (seq
    (loop [l left
           r right
           merged []]
      (cond
        (and (empty? l) (empty? r)) merged
        (empty? l) (concat merged r)
        (empty? r) (concat merged l)
        (<= (first l) (first r)) (recur (rest l) r (conj merged (first l)))
        (> (first l) (first r)) (recur l (rest r) (conj merged (first r)))
        :else (throw (IllegalStateException. "Unexpected merge state."))))))

(defn merge-sort [s]
  (let [length (count s)]
    (seq
      (if (<= length 1)
        s
        (let [parts (split-at (/ length 2) s)
              left (merge-sort (first parts))
              right (merge-sort (second parts))]
          (merge-sorted left right))))))
