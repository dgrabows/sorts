(defproject sorts "0.1.0-SNAPSHOT"
  :description "Playing around with sort algorithms."
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.4.0"]])
